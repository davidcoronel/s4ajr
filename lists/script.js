
nw.require("nwjs-j5-fix").fix();
var gui = require('nw.gui');
var win = gui.Window.get();
win.showDevTools();
win.maximize();


var five = require("johnny-five");
var board = new five.Board();
//var board = new five.Board({ port : '\\\\.\\COM18' });
//var board = new five.Board({ port : '/dev/ttyACM0' });
var led_salida1,
	led_salida2,
	led_salida3,
	led_salida4,
	servo_1_3,
	servo_2_4,
	mot_1_a,
	mot_1_b,
	mot_2_a,
	mot_2_b,
	piezo;


console.log("!!!!");	
board.on("ready", function() {
	led_salida1 = new five.Led(10);
	led_salida2 = new five.Led(11);
	led_salida3 = new five.Led(12);
	led_salida4 = new five.Led(13);
	servo_1_3   = new five.Servo(4);
	servo_2_4	= new five.Servo(9);
	mot_1_a     = new five.Led(5);
	mot_1_b     = new five.Led(7);
	mot_2_a     = new five.Led(6);
	mot_2_b     = new five.Led(8);
	piezo       = new five.Piezo(3);
});

function toggle(){
	led_salida1.toggle();
	led_salida2.toggle();
	led_salida3.toggle();
	led_salida4.toggle();
}

function clearProcedures(){
	$("#cart ol").empty();
}

$(function() {
  $("#catalog").accordion();
  $("#catalog li").draggable({
    appendTo: "body",
    helper: "clone"
  });

  $("#catalog li").click(function() {
    $(this).clone().appendTo("#cart ol");
  });


  $("#cart ol").droppable({
    activeClass: "ui-state-default",
    hoverClass: "ui-state-hover",
    accept: ":not(.ui-sortable-helper)",
    drop: function(event, ui) {
      $(this).find(".placeholder").remove();
	  var id= ui.draggable.attr('id');
      $("<li id='" + ui.draggable.attr('id') + "'></li>").text(ui.draggable.text()).appendTo(this);
	  
    }
  }).sortable({
    items: "li:not(.placeholder)",
    sort: function() {
      // gets added unintentionally by droppable interacting with sortable
      // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
      $(this).removeClass("ui-state-default");
    }
  });


  $('#trash').droppable({
    drop: function(event, ui) {
      if (ui.draggable.parent().attr('id') !== "originals") {
        ui.draggable.remove();
      }
    }
  });

});

var slots = [];
var stop = false;

function kill(){
	stop = true;
	led_salida1.off();
	led_salida2.off();
	led_salida3.off();
	led_salida4.off();
	servo_1_3.off();
	servo_2_4.off();
	mot_1_a.off();
	mot_1_b.off();
	mot_2_a.off();
	mot_2_b.off();
	
}

function run(i){
	
	slots = $("#cart").find('li');
	
	if(i >= slots.length || stop){
		stop = false;
		return;
	}
	
	switch( parseInt( slots[i].id ) ){
		case 0:
			led_salida1.on();
			run(++i);
			break;
		case 1:
			led_salida1.off();
			run(++i);
			break;
		case 2:
			led_salida2.on();
			run(++i);
			break;
		case 3:
			led_salida2.off();
			run(++i);
			break;
		case 4:
			led_salida3.on();
			run(++i);
			break;
		case 5:
			led_salida3.off();
			run(++i);
			break;
		case 6:
			led_salida4.on();
			run(++i);
			break;
		case 7:
			led_salida4.off();
			run(++i);
			break;
		case 20:
			servo_1_3.to(0);
			run(++i);
			break;
		case 21:
			servo_1_3.to(180);
			run(++i);
			break;
		case 22:
			run(++i);
			break;
		case 23:
			run(++i);
			break;
		case 30:
			mot_1_a.on();;
			mot_1_b.off();;
			run(++i);
			break;
		case 31:
			mot_1_a.off();
			mot_1_b.on();
			run(++i);
			break;
		case 32:
			mot_1_a.off();;
			mot_1_b.off();;
			run(++i);
			break;
		case 33:
			mot_2_a.on();;
			mot_2_b.off();;
			run(++i);
			break;
		case 34:
			mot_2_a.off();
			mot_2_b.on();
			run(++i);
			break;
		case 35:
			mot_2_a.off();;
			mot_2_b.off();;
			run(++i);
			break;
		case 40:
			piezo.frequency(262, 20);
			setTimeout(function(){
				piezo.off();
				run(++i);
			},20);
			break;
		case 41:
			piezo.frequency(330, 20);
			setTimeout(function(){
				piezo.off();
				run(++i);
			},20);
			break;
		case 42:
			piezo.frequency(392, 20);
			setTimeout(function(){
				piezo.off();
				run(++i);
			},20);
			break;
		case 43:
			piezo.frequency(523, 20);
			setTimeout(function(){
				piezo.off();
				run(++i);
			},20);
			break;
		case 51:
			setTimeout(function(){run(++i);},1000);
			break;
		case 52:
			setTimeout(function(){run(++i);},2000);
			break;
		case 53:
			setTimeout(function(){run(++i);},5000);
			break;
		case 54:
			setTimeout(function(){run(++i);},10000);
			break;
		case 50:
			run(0);
			break;
	}		
	
}
